#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: serializer.py
# author: liuyu
# date: 2022/5/22
from rest_framework import serializers

from api import models
from common.base.baseutils import OpenIdCrypt


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserInfo
        fields = '__all__'


class WeChatInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.WeChatInfo
        exclude = ['session_key', 'id']

    openid = serializers.SerializerMethodField()
    unionid = serializers.SerializerMethodField()

    def get_openid(self, obj):
        return OpenIdCrypt().get_encrypt_uid(obj.openid)

    def get_unionid(self, obj):
        if obj.unionid:
            return OpenIdCrypt().get_encrypt_uid(obj.unionid)
        return obj.unionid


class PictureInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PictureInfo
        exclude = ['user_id']

    media_preview_url = serializers.SerializerMethodField()

    def get_media_preview_url(self, obj):
        storage = self.context.get('storage')
        if storage:
            return storage.get_download_url(obj.media_name)
        return ''
