#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: utils
# author: liuyu
# date: 2022/5/24
import time

from api.models import WeChatInfo
from common.base.baseutils import make_uuid5
from common.cache.storage import UploadTmpFileNameCache


def upload_file_tmp_name(act, filename, user_obj_id):
    cache_obj = UploadTmpFileNameCache(filename)
    if act == "set":
        cache_obj.del_storage_cache()
        cache_obj.set_storage_cache({'u_time': time.time(), 'id': user_obj_id, "filename": filename}, 25 * 60 * 60)
    elif act == "get":
        return cache_obj.get_storage_cache()
    elif act == "del":
        cache_obj.del_storage_cache()


def make_applet_token(openid):
    session_obj = WeChatInfo.objects.filter(openid=openid).first()
    return make_uuid5(f'{session_obj.openid}:{session_obj.session_key}')
