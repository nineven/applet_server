from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from common.base.baseutils import make_random_uuid


class UserInfo(AbstractUser):
    username = models.CharField("用户名", max_length=64, unique=True)
    nick_name = models.CharField("昵称", max_length=64, null=True, blank=True)
    is_active = models.BooleanField(default=True, verbose_name="账户状态，默认启用")

    job = models.TextField("职位", max_length=128, blank=True, null=True)
    company = models.CharField("公司", max_length=128, blank=True, null=True)

    gender_choices = ((0, '保密'), (1, '男'), (2, '女'))
    gender = models.SmallIntegerField(choices=gender_choices, default=0, verbose_name="性别")
    head_img = models.CharField(max_length=256, default='head_img.jpeg',
                                verbose_name="个人头像")
    role_choices = ((0, '普通会员'), (1, 'VIP'), (2, 'SVIP'), (3, '管理员'))
    role = models.SmallIntegerField(choices=role_choices, default=0, verbose_name="角色")

    memo = models.TextField('备注', blank=True, null=True, default=None, )
    created_time = models.DateTimeField(auto_now_add=True, verbose_name="注册时间")
    wechat = models.OneToOneField(to='WeChatInfo', on_delete=models.CASCADE, verbose_name="微信信息")

    class Meta:
        verbose_name = '账户信息'
        verbose_name_plural = "账户信息"

    def __str__(self):
        return f"{self.nick_name}-{self.get_role_display()}"

    def save(self, *args, **kwargs):
        if len(self.username) < 8:
            self.username = make_random_uuid()
        super(UserInfo, self).save(*args, **kwargs)


class WeChatInfo(models.Model):
    openid = models.CharField(max_length=64, unique=True, verbose_name="普通用户的标识，对当前公众号唯一")
    session_key = models.CharField(verbose_name="session key", max_length=64)
    unionid = models.CharField(verbose_name="unionid", max_length=64, null=True, blank=True)
    mobile = models.BigIntegerField(verbose_name="手机", help_text="用于手机登录", unique=True, blank=True, null=True)
    updated_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    nickname = models.CharField(max_length=64, verbose_name="昵称", blank=True)
    sex = models.SmallIntegerField(default=0, verbose_name="性别", help_text="值为1时是男性，值为2时是女性，值为0时是未知")
    head_img_url = models.CharField(max_length=256, verbose_name="用户头像", blank=True, null=True)
    address = models.CharField(max_length=128, verbose_name="地址", blank=True, null=True)

    class Meta:
        verbose_name = '微信信息'
        verbose_name_plural = "微信信息"

    def __str__(self):
        return f"{self.nickname}-{self.openid}"


class UploadMediaBase(models.Model):
    user_id = models.ForeignKey(to=UserInfo, on_delete=models.CASCADE, verbose_name="用户ID")
    media_name = models.CharField(max_length=128, unique=True, verbose_name="媒体唯一名称")
    created_time = models.DateTimeField(auto_now_add=True, verbose_name="添加时间")

    class Meta:
        abstract = True


class UploadMediaTemp(UploadMediaBase):
    class Meta:
        verbose_name = '上传临时表'
        verbose_name_plural = "上传临时表"

    def __str__(self):
        return f'{self.user_id.nick_name}-{self.media_name}-{self.created_time}'


class UploadMediaInfo(UploadMediaBase):
    media_size = models.IntegerField(verbose_name="媒体大小")
    media_type = models.CharField(max_length=64, verbose_name="媒体类型")
    image_height = models.IntegerField(verbose_name="图片高度", default=0)
    image_width = models.IntegerField(verbose_name="图片宽度", default=0)
    image_format = models.CharField(max_length=32, verbose_name="图片格式，例如jpg、png", default='')

    class Meta:
        abstract = True


class PictureInfo(UploadMediaInfo):
    class Meta:
        verbose_name = '工作信息表'
        verbose_name_plural = "工作信息表"

    def __str__(self):
        return f'{self.user_id.nick_name}-{self.media_name}-{self.media_size}'


class WorkNotes(models.Model):
    user_id = models.ForeignKey(to=UserInfo, on_delete=models.CASCADE, verbose_name="用户ID")
    title = models.CharField(max_length=128, null=False, verbose_name="工作简述")
    content = models.CharField(max_length=4096, null=False, verbose_name="详细工作内容")
    goods_name = models.CharField(max_length=128, null=False, verbose_name="货物名称")

    goods_pic = models.ManyToManyField(to=PictureInfo, verbose_name="货物图片地址", related_name='p_goods_pic')
    goods_weight = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="货物重量，单位 Kg, 精确到2位小数")
    goods_weight_pic = models.ManyToManyField(to=PictureInfo, verbose_name="货物重量图片地址",
                                              related_name='p_goods_weight_pic')
    truck_total_weight = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="货车总重量，单位 Kg, 精确到2位小数")
    truck_total_weight_pic = models.ManyToManyField(to=PictureInfo, verbose_name="货车总重图片地址",
                                                    related_name='p_truck_total_weight_pic')
    truck_net_weight = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="货车净重量，单位 Kg, 精确到2位小数")
    truck_net_weight_pic = models.ManyToManyField(to=PictureInfo, verbose_name="货车净重图片地址",
                                                  related_name='p_truck_net_weight_pic')

    license_plate = models.CharField(verbose_name="车牌", max_length=16)  # 后期可以通过机器学习自动识别车牌
    license_plate_pic = models.ManyToManyField(to=PictureInfo, verbose_name="货车车牌图片地址",
                                               related_name='p_license_plate_pic')
    operate_user_sign_pic = models.ManyToManyField(to=PictureInfo, verbose_name="经办人签字图片地址",
                                                   related_name='p_operate_user_sign_pic')

    arrival_time = models.DateTimeField(verbose_name="进厂时间", null=True, blank=True)
    discharge_time = models.DateTimeField(verbose_name="卸货时间", null=True, blank=True)
    departure_time = models.DateTimeField(verbose_name="离厂时间", null=True, blank=True)

    created_time = models.DateTimeField(auto_now_add=True, verbose_name="添加时间")
    updated_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        verbose_name = '工作信息表'
        verbose_name_plural = "工作信息表"

    def __str__(self):
        return f'{self.user_id.nick_name}-{self.title}'
