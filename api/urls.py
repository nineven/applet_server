"""ICMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from rest_framework.routers import SimpleRouter

from api.views.login import LoginView, RegisterView
from api.views.picture import PictureView
from api.views.upload import UploadView, Upload2View, AliOSSCallBackView
from api.views.wechat import MiniProgramView, MiniProgramDecryptView, MiniProgramRegisterView, MiniProgramUserInfoView

router = SimpleRouter(False)
router.register('picture', PictureView)

urlpatterns = [
    path('register', RegisterView.as_view()),
    path('login', LoginView.as_view()),
    path('upload', UploadView.as_view()),
    path('upload2', Upload2View.as_view()),
    path('callback', AliOSSCallBackView.as_view()),
    path('applet', MiniProgramView.as_view()),
    path('applet/decrypt', MiniProgramDecryptView.as_view()),
    path('applet/register', MiniProgramRegisterView.as_view()),
    path('applet/userinfo', MiniProgramUserInfoView.as_view()),
    path('', include(router.urls))
]
