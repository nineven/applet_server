#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# author: liuyu
# date: 2022/5/22
import datetime
import logging

from rest_framework.views import APIView

from ICMS import settings
from api.models import PictureInfo, UploadMediaTemp
from common.base.baseutils import make_from_user_uuid
from common.core.response import ApiResponse
from common.libs.storage import Storage, Storage2
from common.utils.token import verify_token, make_token

logger = logging.getLogger(__name__)


def upload_put_base(request, storage):
    access_token = request.data.get('access_token')
    upload_key = request.data.get('upload_key')
    if verify_token(token=access_token, unique_token_key=upload_key, success_once=True):
        try:
            obj = PictureInfo.objects.filter(user_id=request.user, media_name=upload_key).first()
            pic_preview_url = storage.get_download_url(obj.media_name)
            return ApiResponse(data={'id': obj.id, 'url': pic_preview_url, 'media_name': obj.media_name})
        except Exception as e:
            storage.delete_file(upload_key)
            return ApiResponse(code=1001, msg=f"异常错误 {e}")
    else:
        return ApiResponse(code=1002, msg="授权失败")


def upload_post_base(request, storage, callback_time_limit=60 * 5):
    filename = request.data.get('filename', '')
    f_type = filename.split(".")[-1]
    if not f_type or (f_type and f_type not in settings.FILE_UPLOAD_ALLOW):
        return ApiResponse(code=1001, msg='上传类型错误')

    random_file = make_from_user_uuid(request.user.username)
    upload_key = storage.format_user_file_path(f"{random_file}.{f_type}")
    access_token = make_token(upload_key, time_limit=callback_time_limit, key='update_file_info', force_new=True)

    clean_time = datetime.datetime.now() - datetime.timedelta(days=2)
    upload_temp_queryset = UploadMediaTemp.objects.filter(user_id=request.user, created_time__lt=clean_time).all()
    for upload_temp in upload_temp_queryset:
        result = storage.delete_file(upload_temp.media_name)
        logger.info(f'{request.user} delete expired temp file {upload_temp.media_name} result:{result}')
        upload_temp.delete()

    UploadMediaTemp.objects.create(user_id=request.user, media_name=upload_key)

    storage.add_callback_params({'access_token': access_token})
    upload_token = storage.get_upload_token(upload_key)

    data = {
        "upload_token": upload_token,
        "upload_key": upload_key,
        "access_token": make_token(upload_key, time_limit=callback_time_limit, key='update_file_info', force_new=True)
    }

    return ApiResponse(data=data)


def upload_delete_base(request, storage):
    pk = request.data.get('id')
    media_name = request.data.get('media_name')
    obj = PictureInfo.objects.filter(user_id=request.user, pk=pk, media_name=media_name).first()
    if obj:
        storage.delete_file(obj.media_name)
        obj.delete()
        return ApiResponse()
    return ApiResponse(code=1003, msg='文件不存在')


class UploadView(APIView):

    def post(self, request):
        """
        获取上传的token
        :param request:
        :return:
        """
        return upload_post_base(request, Storage(request.user), callback_time_limit=60 * 5)

    def put(self, request):
        """
        上传完成之后，更新信息
        :param request:
        :return:
        """
        return upload_put_base(request, Storage(request.user))

    def delete(self, request):
        return upload_delete_base(request, Storage(request.user))


class Upload2View(APIView):

    def post(self, request):
        """
        获取上传的token
        :param request:
        :return:
        """
        return upload_post_base(request, Storage2(request.user), callback_time_limit=60 * 5)

    def put(self, request):
        """
        上传完成之后，更新信息
        :param request:
        :return:
        """
        return upload_put_base(request, Storage2(request.user))

    def delete(self, request):
        return upload_delete_base(request, Storage2(request.user))


class AliOSSCallBackView(APIView):
    authentication_classes = []

    def post(self, request):
        storage = Storage2()
        if storage.callback_verify(request.META, request.body):
            call_data = request.data
            media_name = call_data.get('media_name')
            access_token = call_data.get('access_token')
            if verify_token(token=access_token, unique_token_key=media_name, success_once=True):
                temp_obj = UploadMediaTemp.objects.filter(media_name=media_name).first()
                if temp_obj:
                    pic_info = {
                        'media_size': call_data.get('media_size'),
                        'media_type': call_data.get('media_type'),
                        'image_height': call_data.get('image_height'),
                        'image_width': call_data.get('image_width'),
                        'image_format': call_data.get('image_format')
                    }
                    PictureInfo.objects.create(user_id=temp_obj.user_id, media_name=media_name, **pic_info)
                    temp_obj.delete()
                    return ApiResponse()
        return ApiResponse(status=400, msg="入库失败")
