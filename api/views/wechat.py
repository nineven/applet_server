#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: wechat
# author: liuyu
# date: 2022/5/24
import logging

from django.conf import settings
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from api.models import WeChatInfo, UserInfo
from api.utils.serializer import WeChatInfoSerializer
from common.base.baseutils import make_random_uuid, OpenIdCrypt
from common.core.response import ApiResponse
from common.libs.wechat.applet import WxAppletLogin, WXBizDataCrypt

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
logger = logging.getLogger(__name__)


class MiniProgramRegisterView(APIView):
    authentication_classes = []

    def post(self, request):

        mobile = request.data.get('mobile')
        openid = request.data.get('openid')
        app_id = request.data.get('app_id')
        userinfo = request.data.get('userinfo')

        if not (mobile and openid and app_id and userinfo):
            return ApiResponse(code=1001, msg="参数异常")

        if app_id != settings.WX_APPLET.get('app_id'):
            return ApiResponse(code=1001, msg="参数异常")

        openid = OpenIdCrypt().get_decrypt_uid(openid)
        wechat_obj = WeChatInfo.objects.filter(openid=openid).first()
        if wechat_obj:
            wechat_obj.mobile = mobile
            wechat_obj.nickname = userinfo.get('nickName')
            wechat_obj.head_img_url = userinfo.get('avatarUrl')
            wechat_obj.sex = userinfo.get('gender', 0)
            wechat_obj.save(update_fields=['mobile', 'nickname', 'head_img_url', 'sex'])
        else:
            return ApiResponse(code=1001, msg="参数异常")

        user = UserInfo.objects.filter(wechat=wechat_obj).first()
        if not user:
            user = UserInfo.objects.create_user(username=f'{make_random_uuid()}{make_random_uuid()}',
                                                nick_name=wechat_obj.nickname,
                                                password=wechat_obj.session_key,
                                                wechat=wechat_obj)
        if user:
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            return ApiResponse(token=token, data=WeChatInfoSerializer(wechat_obj).data)
        else:
            return ApiResponse(1001, msg='注册失败')


class MiniProgramUserInfoView(APIView):

    def post(self, request):
        user = request.user
        app_id = request.data.get('app_id')
        if app_id != settings.WX_APPLET.get('app_id'):
            return ApiResponse(code=1001, msg="参数异常")
        return ApiResponse(data=WeChatInfoSerializer(user.wechat).data)


class MiniProgramView(APIView):
    authentication_classes = []

    def get(self, request):
        return ApiResponse()

    def post(self, request) -> ApiResponse:
        """
        通过微信code获取 open_id
        :param request:
        :return:
        """
        logger.info(f"{request.data}")
        auth_code = request.data.get('auth_code')
        app_id = request.data.get('app_id')
        wx_login_obj = WxAppletLogin()
        if app_id == wx_login_obj.app_id:
            session_result = wx_login_obj.get_session_from_code(auth_code)
            if session_result.get('openid'):
                openid = session_result.get('openid')
                unionid = session_result.get('unionid')
                session_key = session_result.get('session_key')
                obj, _ = WeChatInfo.objects.update_or_create(defaults={'session_key': session_key}, openid=openid,
                                                             unionid=unionid)
                logger.error(obj.__dict__)
                return ApiResponse(openid=OpenIdCrypt().get_encrypt_uid(openid))
        return ApiResponse(code=1001)


class MiniProgramDecryptView(APIView):
    authentication_classes = []

    def post(self, request):

        encrypted_data = request.data.get('encryptedData')
        iv = request.data.get('iv')
        openid = request.data.get('openid')
        logging.info(f"{request.data}")
        if not (iv and openid and encrypted_data):
            return ApiResponse(code=1001, msg='参数异常')
        openid = OpenIdCrypt().get_decrypt_uid(openid)
        session = WeChatInfo.objects.filter(openid=openid).first()
        try:
            result = WXBizDataCrypt(session.session_key).decrypt(encrypted_data, iv)
        except Exception as e:
            logger.error(f'decrypt failed {e}')
            return ApiResponse(code=401, msg="需要登录")
        """
        getPhoneNumber: {'phoneNumber': '15738962953', 'purePhoneNumber': '15738962953', 'countryCode': '86',
               'watermark': {'timestamp': 1653390722, 'appid': 'wxb65d1894331e9ff3'}}
        """
        return ApiResponse(data=result)
