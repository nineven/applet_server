#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: picture
# author: liuyu
# date: 2022/5/27
import logging

from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

from api.models import PictureInfo
from api.utils.serializer import PictureInfoSerializer
from common.core.response import ApiResponse
from common.libs.storage import Storage2

logger = logging.getLogger(__name__)


class PageNumber(PageNumberPagination):
    page_size = 20  # 每页显示多少条
    page_size_query_param = 'size'  # URL中每页显示条数的参数
    page_query_param = 'page'  # URL中页码的参数
    max_page_size = 100  # 每页最大100条


class PictureFilter(filters.FilterSet):
    min_media_size = filters.NumberFilter(field_name="media_size", lookup_expr='gte')
    max_media_size = filters.NumberFilter(field_name="media_size", lookup_expr='lte')

    class Meta:
        model = PictureInfo
        fields = ["id", "media_type", "image_format"]


class PictureView(ModelViewSet):
    serializer_class = PictureInfoSerializer
    pagination_class = PageNumber
    queryset = PictureInfo.objects.order_by('-created_time')
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering_fields = ['created_time', 'media_size', 'image_height', 'image_width']
    filterset_class = PictureFilter

    def get_serializer_context(self):
        result = super(PictureView, self).get_serializer_context()
        result['storage'] = Storage2(self.request.user)
        return result

    def list(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(user_id=request.user).all()
        data = super().list(request, *args, **kwargs).data
        return ApiResponse(data=data)

    def destroy(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(user_id=request.user).all()
        instance = self.get_object()
        storage = Storage2(self.request.user)
        res = storage.delete_file(instance.media_name)
        logger.info(f'{request.user} delete media instance.media_name res:{res}')
        self.perform_destroy(instance)
        return ApiResponse()

    def update(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(user_id=request.user).all()
        data = super().update(request, *args, **kwargs).data
        return ApiResponse(data=data)
