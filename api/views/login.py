#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: login
# author: liuyu
# date: 2022/5/22
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from api.models import UserInfo
from api.utils.serializer import UserInfoSerializer
from common.base.baseutils import make_random_uuid
from common.core.response import ApiResponse

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class RegisterView(APIView):
    authentication_classes = []

    def post(self, request):
        mobile = request.data.get('mobile')
        nick_name = request.data.get('nick_name')
        password = request.data.get('password')
        if mobile and password:
            user = UserInfo.objects.create_user(username=f'{make_random_uuid()}{make_random_uuid()}',
                                                nick_name=nick_name,
                                                mobile=mobile, password=password)
            if user:
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                return ApiResponse(token=token, data=UserInfoSerializer(user).data)
            else:
                return ApiResponse(1001, msg='登陆失败')
        return ApiResponse(1002, msg='非法请求')


class LoginView(APIView):
    authentication_classes = []

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            return ApiResponse(data={'token': token})
        else:
            return ApiResponse(1001, msg='登陆失败')
