#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: dowork
# author: liuyu
# date: 2022/5/22
from rest_framework.views import APIView

from common.core.response import ApiResponse


class DoWorkView(APIView):
    authentication_classes = []

    def get(self, request):
        return ApiResponse()

    def post(self, request):
        return ApiResponse()
