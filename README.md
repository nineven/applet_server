## Applet Server API

基于Django实现的微信小程序登录上传图片 api 接口模板

使用以下模块

```
Django==3.2.13
django-cors-headers==3.12.0
django-redis==5.2.0
djangorestframework==3.13.1
djangorestframework-jwt==1.11.0
aliyun-python-sdk-sts==3.1.0
oss2==2.15.0
pycryptodome==3.14.1
```

## 安装使用

### 克隆代码到你本地

```shell
git clone  https://gitee.com/nineven/applet_server.git
```

### 找到settings.py 并打开，进行配置修改

```shell
DATABASES   #数据库配置信息
CACHES      #Redis缓存配置
WX_APPLET   #微信小程序授权
STORAGE     #阿里云存储授权
```

### 迁移表并启动服务

```shell
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
```