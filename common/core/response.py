#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# author: liuyu
# date: 2022/5/22

from rest_framework.response import Response


class ApiResponse(Response):
    def __init__(self, code=1000, msg='success', data=None, status=None, headers=None, content_type=None, **kwargs):
        dic = {
            'code': code,
            'msg': msg
        }
        if data is not None:
            dic['data'] = data
        dic.update(kwargs)
        self._data = data
        # 对象来调用对象的绑定方法，会自动传值
        super().__init__(data=dic, status=status, headers=headers, content_type=content_type)
