#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: 3月 
# author: NinEveN
# date: 2021/3/25


from rest_framework.throttling import SimpleRateThrottle


class LoginUserThrottle(SimpleRateThrottle):
    """登录用户访问频率限制"""
    scope = "LoginUser"

    def get_cache_key(self, request, view):
        if hasattr(request.user, 'username'):
            return request.user.username
        else:
            self.get_ident(request)
