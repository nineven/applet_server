#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: 9月
# author: NinEveN
# date: 2020/9/24
import logging

import jwt
from django.http.cookie import parse_cookie
from rest_framework.exceptions import AuthenticationFailed
# from rest_framework_jwt.authentication import get_authorization_header, jwt_get_username_from_payload
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication
from rest_framework_jwt.authentication import jwt_decode_handler

logger = logging.getLogger(__name__)


def get_cookie_token(request):
    cookies = request.META.get('HTTP_COOKIE')
    if cookies:
        cookie_dict = parse_cookie(cookies)
        return cookie_dict.get('auth_token')


def get_authorization_header(request):
    return request.META.get("HTTP_AUTHORIZATION",
                            request.META.get("HTTP_X_TOKEN",
                                             request.query_params.get("token", get_cookie_token(request))))


class JSONWebTokenAuthentication(BaseJSONWebTokenAuthentication):
    def authenticate(self, request):
        if request.method == "OPTIONS":
            return None

        jwt_value = get_authorization_header(request)

        if not jwt_value:
            raise AuthenticationFailed({"code": 1001, "msg": "token不合法"})
        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed({"code": 1002, "msg": "token已过期"})
        except jwt.InvalidTokenError:
            raise AuthenticationFailed({"code": 1003, "msg": "token已失效"})
        except Exception as e:
            logger.error(f'AuthenticationFailed Exception:{e}')
            raise AuthenticationFailed({"code": 1004, "msg": "token异常"})
        user_obj = self.authenticate_credentials(payload)
        if not user_obj.is_active:
            raise AuthenticationFailed({"code": 1004, "msg": "用户被禁用"})
        return user_obj, jwt_value
