#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: applet
# author: liuyu
# date: 2022/5/24
import base64
import json
import logging

import requests
from Crypto.Cipher import AES
from django.conf import settings

logger = logging.getLogger(__name__)


class WXBizDataCrypt(object):
    def __init__(self, session_key):
        self.app_id = settings.WX_APPLET.get('app_id')
        self.session_key = session_key

    def decrypt(self, encrypted_data, iv):
        # base64 decode
        session_key = base64.b64decode(self.session_key)
        encrypted_data = base64.b64decode(encrypted_data)
        iv = base64.b64decode(iv)

        cipher = AES.new(session_key, AES.MODE_CBC, iv)

        decrypted = json.loads(self._unpad(cipher.decrypt(encrypted_data)))
        logger.info(f"decrypt data: {decrypted}  encrypted_data:{encrypted_data}")
        if decrypted['watermark']['appid'] != self.app_id:
            raise Exception('Invalid Buffer')

        return decrypted

    def _unpad(self, s):
        return s[:-ord(s[len(s) - 1:])]


class WxAppletLogin(object):
    """
    https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
    """

    def __init__(self):
        self.openid = None
        self.access_token = None
        auth_info = settings.WX_APPLET
        self.app_id = auth_info.get('app_id')
        self.app_secret = auth_info.get('app_secret')

    def get_session_from_code(self, js_code):
        """
        登录凭证校验。通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程
        :param js_code:
        """
        t_url = f'https://api.weixin.qq.com/sns/jscode2session?appid={self.app_id}&secret={self.app_secret}&js_code={js_code}&grant_type=authorization_code'
        req = requests.get(t_url)
        if req.status_code == 200:
            logger.info(f"get wx applet session {req.status_code} {req.text}")
            auth_info = req.json()
            return auth_info
        return {}

    def get_access_token(self):
        """
        获取小程序全局唯一后台接口调用凭据（access_token）。调用绝大多数后台接口时都需使用 access_token，开发者需要进行妥善保存
        """
        t_url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&app_id={self.app_id}&secret={self.app_secret}'
        req = requests.get(t_url)
        if req.status_code == 200:
            logger.info(f"get access token {req.status_code} {req.text}")
            auth_info = req.json()
            self.access_token = auth_info.get('access_token')
            return auth_info
        return {}

    def get_phone_number(self, access_token, js_code):
        t_url = f'https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token={access_token}'
        req = requests.post(t_url, json={'code': js_code})
        if req.status_code == 200:
            logger.info(f"get phone number {req.status_code} {req.text}")
            return req.json()
        return {}
