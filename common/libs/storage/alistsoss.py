#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# author: liuyu
# date: 2022/5/22

# pip install aliyun-python-sdk-sts oss2
import base64
import json
import logging

import oss2
from aliyunsdkcore import client
from aliyunsdksts.request.v20150401 import AssumeRoleRequest

from common.base.magic import MagicCacheData
from common.libs.storage.aliossbase import AliYunOssBase, StsToken

logger = logging.getLogger(__name__)


def get_storage_cache_key(*args, **kwargs):
    obj = args[0]
    auth = {
        'access_key_id': obj.access_key_id,
        'access_key_secret': obj.access_key_secret,
        'bucket_name': obj.bucket_name,
        'endpoint': obj.endpoint
    }
    return base64.b64encode(json.dumps(auth).encode("utf-8")).decode("utf-8")[0:64]


class AliYunOss(AliYunOssBase):

    def __init__(self, access_key_id, access_key_secret, bucket_name, endpoint, is_https, download_auth_type=1,
                 callback_url=None,
                 sts_role_arn=None, domain_name=None, cnd_auth_key=None):
        super().__init__(access_key_id, access_key_secret, bucket_name, endpoint, is_https, download_auth_type,
                         callback_url,
                         sts_role_arn, domain_name, cnd_auth_key)

    def __get_bucket(self):
        token = self.make_sts_token(3600)
        auth = oss2.StsAuth(token.access_key_id, token.access_key_secret, token.security_token)
        bucket = self.get_auth_bucket(auth)
        return bucket

    @MagicCacheData.make_cache(3600, 1800, key=get_storage_cache_key)
    def make_sts_token(self, expires):
        return self.fetch_sts_token(expires)

    def make_put_sts_token(self, name, expires):
        p_action = ["oss:PutObject", "oss:AbortMultipartUpload"]
        # https://help.aliyun.com/document_detail/28763.htm?spm=a2c4g.11186623.0.0.74c958eetsLUlS#reference-clc-3sv-xdb
        # https://help.aliyun.com/document_detail/93738.htm?spm=a2c4g.11186623.0.0.59956dc13ePk4x
        policy = {
            "Statement": [
                {
                    "Action": p_action,
                    "Effect": "Allow",
                    "Resource": [f"acs:oss:*:*:{self.bucket_name}/{name}"]
                }
            ],
            "Version": "1",
        }
        return self.fetch_sts_token(expires, name, policy)

    def fetch_sts_token(self, expires, name=None, policy=None):
        """子用户角色扮演获取临时用户的密钥
        :param name:
        :param expires: 过期时间
        :param policy: policy
        :return StsToken: 临时用户密钥
        """
        region_id = '-'.join(self.endpoint.split('.')[0].split("-")[1:3])
        clt = client.AcsClient(self.access_key_id, self.access_key_secret, region_id)
        req = AssumeRoleRequest.AssumeRoleRequest()

        req.set_accept_format('json')
        req.set_RoleArn(self.sts_role_arn)
        if name:
            role_session_name = base64.b64encode(name.encode()).decode()[:16]
        else:
            role_session_name = self.access_key_id
        req.set_RoleSessionName(role_session_name)
        req.set_DurationSeconds(expires)
        if policy:
            req.set_Policy(json.dumps(policy))
        body = clt.do_action_with_exception(req)
        j = json.loads(oss2.to_unicode(body))
        token = StsToken()
        token.access_key_id = j['Credentials']['AccessKeyId']
        token.access_key_secret = j['Credentials']['AccessKeySecret']
        token.security_token = j['Credentials']['SecurityToken']
        token.expiration = oss2.utils.to_unixtime(j['Credentials']['Expiration'], '%Y-%m-%dT%H:%M:%SZ')
        token.bucket = self.bucket_name
        token.endpoint = self.endpoint
        token.callback = self.get_callback_body()
        token.callback_var = self.get_callback_var()
        token.host = self.get_host()
        logger.info(f"get aliyun sts token {token.__dict__}")
        return token

    def get_upload_token(self, name, expires=1800, content_max_length=10 * 1024 * 1024):
        token = self.make_put_sts_token(name, expires)
        token.content_max_length = content_max_length
        return token.__dict__
