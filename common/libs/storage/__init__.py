#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: __init__
# author: liuyu
# date: 2022/5/24
import logging

from django.conf import settings

from common.base.magic import MagicCacheData
from .alicdn import AliYunCdn
from .alioss import AliOss
from .alistsoss import AliYunOss

logger = logging.getLogger(__name__)


def get_oss_storage(oss_func):
    for storage in settings.ALIYUNOSS:
        if storage.get("active", None):
            auth = storage.get('auth', {})
            return oss_func(**auth)
    return None


class Storage(object):
    def __init__(self, user=None):
        self.user = user
        self.storage = AliYunOss(**settings.ALIYUNOSS)

    def add_callback_params(self, params_dict):
        if self.storage:
            self.storage.callback_params = params_dict

    def format_user_file_path(self, filename):
        user_dir = ""
        if self.user:
            user_dir = f"{self.user.pk}/"
        return f"{user_dir}{filename}"

    def get_upload_token(self, filename, expires=900, content_max_length=10 * 1024 * 1024):
        if self.storage:
            return self.storage.get_upload_token(filename, expires, content_max_length)

    @MagicCacheData.make_cache(900, 180, key=lambda *args: args[1])
    def get_download_url(self, filename, expires=900):
        if self.storage:
            if self.storage.__dict__.get("download_auth_type", None) == 2:
                cdn_obj = AliYunCdn(self.storage.cnd_auth_key, self.storage.is_https, self.storage.domain_name)
                download_url = cdn_obj.get_cdn_download_token(filename, expires)
            else:
                download_url = self.storage.get_download_url(filename, expires, force_new=True)
            return download_url

    def delete_file(self, filename):
        if self.storage:
            try:
                logger.warning(f"storage {self.storage} delete file  {filename}")
                return self.storage.del_file(filename)
            except Exception as e:
                logger.error(f"delete file {filename} failed  Exception {e}")

    def rename_file(self, old_filename, new_filename):
        if self.storage:
            try:
                return self.storage.rename_file(old_filename, new_filename)
            except Exception as e:
                logger.error(f"rename {old_filename} to {new_filename} failed  Exception {e}")

    def get_file_info(self, filename):
        if self.storage:
            try:
                return self.storage.get_file_info(filename)
            except Exception as e:
                logger.error(f"get file info {filename} failed  Exception {e}")

    def upload_file(self, local_file_full_path):
        if self.storage:
            try:
                return self.storage.upload_file(local_file_full_path)
            except Exception as e:
                logger.error(f"oss upload  {local_file_full_path} failed  Exception {e}")

    def download_file(self, file_name, local_file_full_path):
        if self.storage:
            try:
                return self.storage.download_file(file_name, local_file_full_path)
            except Exception as e:
                logger.error(f"oss download  {local_file_full_path} failed  Exception {e}")

    def callback_verify(self, request_headers, request_body):
        if self.storage:
            try:
                return self.storage.callback_verify(request_headers, request_body)
            except Exception as e:
                logger.error(f"oss callback verify failed  Exception {e}")


class Storage2(Storage):
    def __init__(self, user=None):
        super().__init__(user)
        self.storage = AliOss(**settings.ALIYUNOSS)
