#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# author: liuyu
# date: 2022/5/22

# pip install aliyun-python-sdk-sts oss2
import base64
import json
import logging
import os
import urllib.request
from hashlib import md5

import oss2
from Crypto.Hash import MD5
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from oss2 import SizedFileAdapter, determine_part_size
from oss2.models import PartInfo

from common.base.magic import MagicCacheData

logger = logging.getLogger(__name__)


# 以下代码展示了STS的用法，包括角色扮演获取临时用户的密钥、使用临时用户的密钥访问OSS。

# STS入门教程请参看  https://yq.aliyun.com/articles/57895
# STS的官方文档请参看  https://help.aliyun.com/document_detail/28627.html

# 首先初始化AccessKeyId、AccessKeySecret、Endpoint等信息。
# 通过环境变量获取，或者把诸如“<你的AccessKeyId>”替换成真实的AccessKeyId等。
# 注意：AccessKeyId、AccessKeySecret为子用户的密钥。
# 子用户需要有  调用STS服务AssumeRole接口的权限
# 创建ram用户，授权 管理对象存储服务（OSS）权限
# RoleArn可以在控制台的“访问控制  > RAM角色管理  > 创建的ram用户  > 基本信息  > Arn”上查看。
#
# 以杭州区域为例，Endpoint可以是：
#   http://oss-cn-hangzhou.aliyuncs.com
#   https://oss-cn-hangzhou.aliyuncs.com
# 分别以HTTP、HTTPS协议访问。

def md5sum(src):
    m = md5()
    m.update(src)
    return m.hexdigest()


def verify_auth(auth_str, authorization_base64, pub_key):
    """
    校验签名是否正确（MD5 + RAS）
    :param auth_str: 文本信息
    :param authorization_base64: 签名信息
    :param pub_key: 公钥
    :return: 若签名验证正确返回 True 否则返回 False
    """
    pub_key_load = RSA.importKey(pub_key)
    auth_md5 = MD5.new(auth_str.encode())
    try:
        return PKCS1_v1_5.new(pub_key_load).verify(auth_md5, base64.b64decode(authorization_base64.encode()))
    except Exception as e:
        logger.error(f"oss callback authorization verify failed! Exception:{e}")


@MagicCacheData.make_cache(3600, 180, key=lambda *args: f"oss_public_key_{args[0]}")
def get_pub_key(pub_key_url_base64):
    """ 抽取出 public key 公钥 """
    pub_key_url = base64.b64decode(pub_key_url_base64.encode()).decode()
    if pub_key_url.find("://gosspublic.alicdn.com/") == -1:
        raise Exception('public key error')
    url_reader = urllib.request.urlopen(pub_key_url)
    pub_key_content = url_reader.read()

    return pub_key_content, pub_key_url


class StsToken(object):

    def __init__(self):
        self.access_key_id = ''
        self.access_key_secret = ''
        self.expiration = 3600
        self.security_token = ''
        self.bucket = ''
        self.endpoint = ''
        self.callback = ''
        self.callback_var = ''
        self.host = ''


# def get_storage_cache_key(*args, **kwargs):
#     a = ''
#     for i in range(1, 6):
#         a = f"{a}{args[i]}"
#     return base64.b64encode(a.encode("utf-8")).decode("utf-8")[0:64]
#

class AliYunOssBase(object):
    def __init__(self, access_key_id, access_key_secret, bucket_name, endpoint, is_https, download_auth_type=1,
                 callback_url=None,
                 sts_role_arn=None, domain_name=None, cnd_auth_key=None):
        self.access_key_id = access_key_id
        self.access_key_secret = access_key_secret
        self.bucket_name = bucket_name
        self.callback_url = callback_url
        self.endpoint = endpoint
        self.sts_role_arn = sts_role_arn
        self.is_https = is_https
        self.download_auth_type = download_auth_type
        self.cnd_auth_key = cnd_auth_key
        self.domain_name = domain_name
        self.callback_params = None
        self.bucket = self.__get_bucket()

    def __get_bucket(self):
        auth = oss2.Auth(self.access_key_id, self.access_key_secret)
        bucket = self.get_auth_bucket(auth)
        return bucket

    def get_host(self):
        return f"{'https://' if self.is_https else 'http://'}{self.bucket_name}.{self.endpoint}"

    def get_auth_bucket(self, auth):
        uri = 'https://' if self.is_https else 'http://'
        url = self.endpoint
        is_cname = False
        if self.domain_name and self.download_auth_type == 1:  # 1 oss 2 cdn
            url = self.domain_name
            is_cname = True
        return oss2.Bucket(auth, f"{uri}{url}", self.bucket_name, is_cname=is_cname)

    def get_download_url(self, name, expires=1800, force_new=False):
        private_url = self.bucket.sign_url('GET', name, expires, slash_safe=True)
        return private_url

    def add_callback_params(self, callback_params):
        self.callback_params = callback_params

    def get_callback_var(self):
        callback_var_dict = {}
        params_dict = self.callback_params if self.callback_params else {}
        if params_dict and isinstance(params_dict, dict):
            for key, value in params_dict.items():
                callback_var_dict[f'x:{key.lower()}'] = value
        return callback_var_dict, base64.b64encode(json.dumps(callback_var_dict).strip().encode()).decode()

    def get_callback_body(self):
        callback_var = ''
        params_dict = self.callback_params if self.callback_params else {}
        if params_dict and isinstance(params_dict, dict):
            for key in params_dict.keys():
                callback_var = f'{callback_var}{key}=${{x:{key.lower()}}}&'
        if callback_var:
            callback_var = f'&{callback_var[:-1]}'
        callback_dict = {
            'callbackUrl': self.callback_url,
            'callbackBody': 'media_name=${object}&media_size=${size}&media_type=${mimeType}&image_height=${imageInfo.height}&image_width=${imageInfo.width}&image_format=${imageInfo.format}' + callback_var,
            'callbackBodyType': 'application/x-www-form-urlencoded'
        }

        return base64.b64encode(json.dumps(callback_dict).strip().encode()).decode()

    def del_file(self, name):
        return self.bucket.delete_object(name)

    def rename_file(self, old_filename, new_filename):
        self.bucket.copy_object(self.bucket_name, old_filename, new_filename)
        return self.del_file(old_filename)

    def upload_file(self, local_file_full_path):
        if os.path.isfile(local_file_full_path):
            filename = os.path.basename(local_file_full_path)
            headers = {
                'Content-Disposition': f'attachment; filename="{filename}"',
                'Cache-Control': ''
            }
            return self.bucket.put_object_from_file(filename, local_file_full_path, headers)
        else:
            logger.error(f"file {local_file_full_path} is not file")

    def download_file(self, name, local_file_full_path):
        dir_path = os.path.dirname(local_file_full_path)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        return self.bucket.get_object_to_file(name, local_file_full_path)

    def get_file_info(self, name):
        result = self.bucket.head_object(name)
        base_info = {}
        if result.content_length:
            base_info['content_length'] = result.content_length
        if result.last_modified:
            base_info['last_modified'] = result.last_modified
        return base_info

    def multipart_upload_file(self, local_file_full_path):
        if os.path.isfile(local_file_full_path):
            total_size = os.path.getsize(local_file_full_path)
            # determine_part_size方法用于确定分片大小。
            part_size = determine_part_size(total_size, preferred_size=1024 * 1024 * 100)
            filename = os.path.basename(local_file_full_path)
            headers = {
                'Content-Disposition': 'attachment; filename="%s"' % filename.encode("utf-8").decode("latin1"),
                'Cache-Control': ''
            }
            # 初始化分片。
            # 如需在初始化分片时设置文件存储类型，请在init_multipart_upload中设置相关headers，参考如下。
            # headers = dict()
            # headers["x-oss-storage-class"] = "Standard"
            upload_id = self.bucket.init_multipart_upload(filename, headers=headers).upload_id
            parts = []

            # 逐个上传分片。
            with open(local_file_full_path, 'rb') as f:
                part_number = 1
                offset = 0
                while offset < total_size:
                    num_to_upload = min(part_size, total_size - offset)
                    # 调用SizedFileAdapter(fileobj, size)方法会生成一个新的文件对象，重新计算起始追加位置。
                    result = self.bucket.upload_part(filename, upload_id, part_number,
                                                     SizedFileAdapter(f, num_to_upload))
                    parts.append(PartInfo(part_number, result.etag))

                    offset += num_to_upload
                    part_number += 1
            return self.bucket.complete_multipart_upload(filename, upload_id, parts)
        else:
            logger.error(f"file {local_file_full_path} is not file")

    def callback_verify(self, request_headers, request_body):
        if self.bucket_name != request_headers['HTTP_X_OSS_BUCKET']:
            logger.error(f"{self.bucket_name} {self.endpoint} bucket_name verify failed!")
            return

        pub_key_url = ''
        pub_key_url_base64 = request_headers.get('HTTP_X_OSS_PUB_KEY_URL')
        try:
            pub_key, pub_key_url = get_pub_key(pub_key_url_base64)
        except Exception as e:
            MagicCacheData.invalid_cache(f"oss_public_key_{pub_key_url_base64}")
            logger.error(
                f"{self.bucket_name} {self.endpoint} get pub key failed! pub_key_url:{pub_key_url} Exception:{e}")
            return

        # get authorization
        authorization_base64 = request_headers['HTTP_AUTHORIZATION']

        # get callback body
        content_length = int(request_headers['CONTENT_LENGTH'])
        callback_body = request_body[:content_length]

        query_string = request_headers['QUERY_STRING']
        path_info = request_headers['PATH_INFO']
        query_string = '?' + query_string if query_string else ''
        auth_str = path_info + query_string + '\n' + callback_body.decode()

        if not verify_auth(auth_str, authorization_base64, pub_key):
            logger.error(f"{self.bucket_name} {self.endpoint} authorization verify failed!")
            return

        return True
