#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: alicdn
# author: liuyu
# date: 2022/5/26
import hashlib
import logging
import re
import time

logger = logging.getLogger(__name__)


def md5sum(src):
    m = hashlib.md5()
    m.update(src)
    return m.hexdigest()


class AliYunCdn(object):
    def __init__(self, key, is_https, domain_name):
        uri = 'https://' if is_https else 'http://'
        self.domain = uri + domain_name
        self.key = key

    # 鉴权方式A
    def a_auth(self, uri, exp):
        p = re.compile("^(http://|https://)?([^/?]+)(/[^?]*)?(\\?.*)?$")
        if not p:
            return None
        m = p.match(uri)
        scheme, host, path, args = m.groups()
        if not scheme: scheme = "http://"
        if not path: path = "/"
        if not args: args = ""
        rand = "0"  # "0" by default, other value is ok
        uid = "0"  # "0" by default, other value is ok
        ss_string = f"{path}-{exp}-{rand}-{uid}-{self.key}"
        hash_value = md5sum(ss_string.encode("utf-8"))
        auth_key = f"{exp}-{rand}-{uid}-{hash_value}"
        if args:
            return f"{scheme}{host}{path}{args}&auth_key={auth_key}"
        else:
            return f"{scheme}{host}{path}{args}?auth_key={auth_key}"

    def get_cdn_download_token(self, filename, expires=1800):
        uri = f"{self.domain}/{filename}"
        exp = int(time.time()) + expires
        download_url = self.a_auth(uri, exp)
        logger.info(f"make cdn download url {download_url}")
        return download_url
