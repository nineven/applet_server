#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: alioss
# author: liuyu
# date: 2022/5/26
import base64
import datetime
import hmac
import json
import logging
import time
from hashlib import sha1 as sha

from common.libs.storage.aliossbase import AliYunOssBase

logger = logging.getLogger(__name__)


def get_iso_8601(expire):
    gmt = datetime.datetime.utcfromtimestamp(expire).isoformat()
    gmt += 'Z'
    return gmt


class AliOss(AliYunOssBase):

    def __init__(self, access_key_id, access_key_secret, bucket_name, endpoint, is_https, download_auth_type=1,
                 callback_url=None,
                 sts_role_arn=None, domain_name=None, cnd_auth_key=None):
        """
        :param bucket_name:
        :param endpoint:
        :param callback_url:
        """

        super().__init__(access_key_id, access_key_secret, bucket_name, endpoint, is_https, download_auth_type,
                         callback_url,
                         sts_role_arn, domain_name, cnd_auth_key)

    def get_upload_token(self, filename, expire_time=900, content_max_length=10 * 1024 * 1024):
        """
        :param filename:   上传文件
        :param expire_time:     token生效时间
        :param content_max_length:  最大上传文件大小
        :return:
        conditions: https://help.aliyun.com/document_detail/31988.htm?spm=a2c4g.11186623.0.0.64f04b784jjZf0#section-d5z-1ww-wdb
        """
        expire_time = int(time.time()) + expire_time
        callback = self.get_callback_body()
        policy_dict = {
            'expiration': get_iso_8601(expire_time),
            'conditions': [
                # ['starts-with', '$key', upload_dir],
                {'bucket': self.bucket_name},
                {'callback': callback},
                ['eq', '$key', filename],
                ["content-length-range", 0, content_max_length]
            ]
        }
        policy = json.dumps(policy_dict).strip()
        policy_encode = base64.b64encode(policy.encode())
        h = hmac.new(self.access_key_secret.encode(), policy_encode, sha)
        sign_result = base64.encodebytes(h.digest()).strip()

        token_dict = {
            'access_key_id': self.access_key_id,
            'host': self.get_host(),
            'policy': policy_encode.decode(),
            'signature': sign_result.decode(), 'expire': expire_time,
            'callback': callback,
            'callback_var': self.get_callback_var(),
        }
        return token_dict

    def get_download_url(self, object_name, timeout=900, force_new=False):
        """
        :param force_new:
        :param object_name: 文件名称 例如 '2/head.jpeg'
        :param timeout:  生成文件下载链接有效期 60 秒
        :return:  返回签名后的URL
        """
        # 生成签名URL时，OSS默认会对Object完整路径中的正斜线（/）进行转义，从而导致生成的签名URL无法直接使用。
        # 设置slash_safe为True，OSS不会对Object完整路径中的正斜线（/）进行转义，此时生成的签名URL可以直接使用。
        return self.bucket.sign_url('GET', object_name, timeout, slash_safe=True)
