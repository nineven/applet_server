#!/usr/bin/env python
# -*- coding:utf-8 -*-
# project: ICMS
# filename: tests
# author: liuyu
# date: 2022/5/25
import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ICMS.settings')
django.setup()

from common.libs.wechat.applet import WXBizDataCrypt

session_key = "8iIZCYnOl5gCLKwyuDJH6g=="
data = {'d_type': 'mobile',
        'encryptedData': 'Gb+G995OWMKqlcrtjptZciZNJB5DKfSfHGm4qHN2sZJUzyjOBZIctpngPoHd+a5Idc3bVjubzi+8cYfq+n7HtyRG1HX1s4QUjvNSa5H0+IOaqOISwnqeV3JPqmO72g0esF2MTRI5nR/6td4zMWfdI64wflkZYNEslE1kqs2Y6e4suc/ezZJ2bPb5l/W1Y758QprRXqYASDNmqiycRC+mgw==',
        'iv': 'KAO6FkEiHkxCuKEULFFy2A=='}
data = {'d_type': 'mobile',
        'encryptedData': '+eQbxW5buFHDJrWyBhrPmp4txDOfzEPl5Y0hcBIL1BRJXK5XROS8gU9ZoRxKw9pNcB/AQ80QGhgXaeg1aFFJYFUcSjSMoe0W5AIRWVj6pCiX2M4RBrXHjRoLS36juiu06y41IoNkAQGEdyIvr+rANV8lsyNRjYsTbw5ds1xW4Z4ZAohYT5EkTixiAhlrZV4gzQ8aiDR7jc3tTwQnY0T4WQ==',
        'iv': 'bh8/9GNHpPTNPXN5gs8Nzg==', 'openid': 'NqUnUfrtzTX1y+e/ZVSHoxF6crw+ss4zpwcJdzw4jKzAweEt5pOvS82G5RZ7LsJL',
        'app_id': 'wxb65d1894331e9ff3'}
result = WXBizDataCrypt(session_key).decrypt(data.get('encryptedData'), data.get('iv'))
print(result)
